from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_napalm.plugins.tasks import napalm_get
if __name__ == '__main__':

    devices = InitNornir(
        config_file="nornir.yaml", dry_run=True
    )

    results = devices.run(
        task=napalm_get, getters=["facts"]
    )

    # results = devices.filter(city='bj').run(
    #     task=napalm_get, getters=["facts"]
    # )
    print_result(results)