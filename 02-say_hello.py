from nornir import InitNornir
from nornir.core.task import Result
from nornir_utils.plugins.functions import print_result


def say_hello(task,sth_2_say='Hello world!'):
    task.host.username
    words = f'{task.host.name} says :{sth_2_say}'
    print(words)
    return Result(
        host=task.host,  # 当前这个执行结果的设备信息
        result=words,  # 执行结果，可以是任意python对象，最终都会以str的方式打印出来，但是不影响我们取出相关信息做一些判断之类的
        # severity_level=logging.DEBUG, # log的level
    )


if __name__ == '__main__':

    nr = InitNornir(
        config_file="nornir.yaml", dry_run=True
    )

    results = nr.run(
        task=say_hello, sth_2_say='你好！'
    )
    print_result(results)
