from nornir import InitNornir
from nornir_netmiko import netmiko_send_command
from nornir.core.task import Result


def get_nornir_obj(devs):
    devs_data = []
    for dev in devs:
        devs_data.append(
            {
                'name': dev['name'],
                'hostname': dev['ip'],
                'platform': dev['platform'],
                'port': 22,
                'username': dev['username'],
                'password': dev['password'],
                'netmiko_secret': dev['secret'],

            }

        )
    runner = {
        "plugin": "threaded",
        "options": {
            "num_workers": 100,
        },
    }
    inventory = {
        "plugin": "FlatDataInventory",
        "options": {
            "data": devs_data,
        },
    }

    nr = InitNornir(runner=runner, inventory=inventory)
    return nr


def show_cmds(task, cmds, use_timing=True, enable=True):
    outputs = []
    for cmd in cmds:
        result = task.run(netmiko_send_command,
                          command_string=cmd, use_timing=use_timing, enable=enable)
        output = result.result
        outputs.append(output)
    return Result(
        host=task.host,  # 当前这个执行结果的设备信息
        result='\n'.join(outputs),  # 执行结果，可以是任意python对象，最终都会以str的方式打印出来，但是不影响我们取出相关信息做一些判断之类的
        # severity_level=logging.DEBUG, # log的level
    )


def ssh_2_show(devs, cmds):
    nr = get_nornir_obj(devs)
    result = nr.run(show_cmds, cmds=cmds)
    return result


if __name__ == '__main__':
    ...
