from django.urls import path
from .views import index,show_cmds
urlpatterns = [
    path('index/', index, name='index'),
    path('showCmds/', show_cmds, name='show_cmds'),
]
