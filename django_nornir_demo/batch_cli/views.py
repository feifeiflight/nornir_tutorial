from django.shortcuts import render
from utils.nornir_ssh_show import ssh_2_show


# Create your views here.

def index(request):
    return render(request, 'batch_cli/index.html')


def show_cmds(request):
    if request.method == 'GET':
        return render(request, 'batch_cli/show_cmds.html')
    else:
        username = request.POST['username']
        password = request.POST['password']
        secret = request.POST['secret']
        device_type = request.POST['device_type']
        ip_list = request.POST['ip_list']
        cmds = request.POST['cmds']
        ''':arg
        'name': dev['name'],
                'hostname': dev['ip'],
                'platform': dev['platform'],
                'port': 22,
                'username': dev['usernmae'],
                'password': dev['password'],
                'netmiko_secret': dev['secret'],
        
        '''
        devs = []
        for ip in ip_list.split():
            devs.append(
                {
                    'name': ip,
                    'ip': ip,
                    'username': username,
                    'password': password,
                    'secret': secret,
                    'platform': device_type,
                }
            )

        result = ssh_2_show(devs, cmds=cmds.splitlines())
        data = []
        for host,m_result in result.items():
            data.append({
                'ip':host,
                'show_content':[r.result for r in m_result]
            })
        return render(request, 'batch_cli/show_cmds_result.html',{'data':data})
