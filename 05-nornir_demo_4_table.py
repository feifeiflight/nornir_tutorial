from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command
import nornir_table_inventory
runner = {
    "plugin": "threaded",
    "options": {
        "num_workers": 4,
    },
}
inventory = {
    "plugin": "ExcelInventory",
    "options": {
        "excel_file": "inventory.xlsx",
    },
}

nr = InitNornir(runner=runner, inventory=inventory)
bj_devs = nr.filter(city='bj')
results = bj_devs.run(task=netmiko_send_command, command_string='show version')
print_result(results)
