from nornir import InitNornir
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file
from nornir_netmiko import netmiko_send_command
from nornir.core.plugins.inventory import InventoryPluginRegister
import netmiko


def show_cmds(task, cmds, use_timing=True, enable=True):
    for cmd in cmds:
        result = task.run(netmiko_send_command,
                          command_string=cmd, use_timing=use_timing, enable=enable)
        output = result.result

        with open(f"{task.host.name}-{cmd.replace(' ', '_')}.log", mode='w', encoding='utf8') as f:
            f.write(output)

    return 'success'


def show_cmds_style_nornir_utils(task, cmds, use_timing=True, enable=True):

    for cmd in cmds:
        result = task.run(netmiko_send_command,
                          command_string=cmd, use_timing=use_timing, enable=enable
                          )
        output = result.result
        task.run(write_file, filename=f"{task.host.name}-{cmd.replace(' ', '_')}_style2.log", content=output)

    return 'success'


if __name__ == '__main__':
    nr = InitNornir(
        config_file="nornir.yaml"
    )
    cmds = ['show ip interface brief', 'show version', 'show running-config']
    cmds = ['show version', ]
    results = nr.run(task=show_cmds, cmds=cmds, use_timing=True, enable=True)
    # print_result(results)

    # results = nr.run(task=show_cmds_style_nornir_utils, cmds=cmds, use_timing=True, enable=True)
    print_result(results)
